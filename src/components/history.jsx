import React from 'react';

export const History = ({ histories }) => {
    // histories = [
    //   { originalUrl, id }
    // ]
    return (
        <div>
            {
                histories.map((history) => {
                    const shortUrl = `${ window.location.origin }/${ history.id }`;
                    return (
                        <div key={ history.id }>
                            <a href={ history.originalUrl } target="_blank" rel="noopener noreferrer">
                                { history.originalUrl.split('/')[2] }...
                            </a>
                            <br />
                            <a href={ shortUrl } target="_blank" rel="noopener noreferrer">{ shortUrl }</a>
                        </div>
                    )
                })
            }
        </div>
    )
}
