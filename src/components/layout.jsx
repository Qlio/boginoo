import React from 'react';
import { Navigation } from './';

import { IconDash, IconEndBracket, IconStartBracket } from '../components/';

export const Layout = ({ children }) => {
    return (
        <div className='flex flex-col items-center pa-3' style={{ width: '100vw', height: '100vh' }}>
            <Navigation />

            <div className='flex justify-center items-center'>
                <IconStartBracket />
                <IconDash />
                <IconEndBracket />
            </div>
            <div className='font-lobster c-primary fs-56 lh-70'>
                Boginoo
            </div>

            <div className='w100 flex-1'>
                { children }
            </div>

            <div className='font-ubuntu fs-16 lh-18'>
                Made with <span role="img" aria-label="heart">♥️</span> by Nest Academy
            </div>
            <div className='font-ubuntu fs-16 lh-18' style={{ opacity: 0.2 }}>
                ©boginoo.io 2020
            </div>
        </div>
    );
};
