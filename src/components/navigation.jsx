import React, { useContext } from 'react';
import { useHistory } from 'react-router-dom';

import { Button } from './';

import { firebaseAuth } from '../firebase';
import { AuthContext } from '../contexts';

export const Navigation = () => {
    const { user } = useContext(AuthContext);
    const history = useHistory();

    const onClickLogIn = () => {
        history.push('/login');
    }

    const onClickLogOut = () => {
        firebaseAuth.signOut();
    }

    return (
        <div className='w100 flex justify-end items-center'>
            <div className='font-ubuntu fs-20 lh-23 bold c-primary'>ХЭРХЭН АЖИЛЛАДАГ ВЭ?</div>
            {
                !user.isAnonymous ? (
                    <Button onClick={ onClickLogOut } className='font-ubuntu fs-20 lh-23 bold c-default h-5 ph-4 ml-4 b-primary'>Гарах</Button>
                ) : (
                    <Button onClick={ onClickLogIn } className='font-ubuntu fs-20 lh-23 bold c-default h-5 ph-4 ml-4 b-primary'>Нэвтрэх</Button>
                )
            }
        </div>
    );
};
