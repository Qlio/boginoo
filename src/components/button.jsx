import React from 'react';

export const Button = ({ children, disabled, className, onClick, ...othersProps }) => {
    return (
        <button
            className={ `btn ${ className } ${ (disabled && 'disabled') || '' }` }
            onClick={ onClick }
            { ...othersProps }
        >
            { children }
        </button>
    );
};
