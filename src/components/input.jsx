import React from 'react';

export const Input = ({ className, value, onChange, ...otherProps }) => {
    return (
        <div>
            <input className={`input ${className}`} value={ value } onChange={ (e) => onChange(e.target.value) } { ...otherProps } />
        </div>
    );
};
