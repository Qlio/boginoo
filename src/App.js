import React, { useState } from 'react';
import { HomeDefault, UrlRedirect, LoginPage, RegisterPage } from './pages';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect,
} from 'react-router-dom';

import { firebaseAuth } from './firebase';
import { Layout } from './components';
import { AuthContext } from './contexts';

import './style/main.scss';

const App = () => {
    const [ user, setUser ] = useState();

    firebaseAuth.onAuthStateChanged((user) => {
        if (!user) {
            firebaseAuth.signInAnonymously();
        }

        setUser(user);
    });

    return (
        <AuthContext.Provider value={{ user, setUser }}>
            <Router>
                {
                    !user ? (
                        <h1 className="font-ubuntu fs-36 c-primary text-center">Loading...</h1>
                    ) : (
                        <Layout>
                            <Switch>
                                <Route path="/" exact>
                                    <HomeDefault />
                                </Route>
                                <Route path="/login">
                                    { user.isAnonymous ? <LoginPage /> : <Redirect to="/" /> }
                                </Route>
                                <Route path="/register">
                                    { user.isAnonymous ? <RegisterPage /> : <Redirect to="/" /> }
                                </Route>
                                <Route path="/:firestoreId">
                                    <UrlRedirect />
                                </Route>
                            </Switch>
                        </Layout>
                    )
                }
            </Router>
        </AuthContext.Provider>
    )
}

export default App
