import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

const firebaseConfig = {
    apiKey: "AIzaSyAyyCimSAvdLB7Fb-DXMvhW6PuUomkN-u8",
    authDomain: "test-79e4e.firebaseapp.com",
    projectId: "test-79e4e",
    storageBucket: "test-79e4e.appspot.com",
    messagingSenderId: "360974497626",
    appId: "1:360974497626:web:031398fab95cf714856563"
};

firebase.initializeApp(firebaseConfig);

export default firebase;
export const firestore = firebase.firestore();
export const firebaseAuth = firebase.auth();
