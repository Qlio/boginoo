import React from 'react';

import { Button, Input } from '../components/';

export const Login = () => {
    return (
        <div className='h100 flex flex-col justify-center items-center'>
            <div className="font-ubuntu">
                <h1 className="text-center c-primary">Нэвтрэх</h1>

                <label className="ph-4" htmlFor="email-input">Цахим хаяг</label>
                <Input id="email-input" className="h-5 w-8 ph-4 mt-2 mb-4" placeholder="name@mail.doman" />

                <label className="ph-4" htmlFor="password-input">Нууц үг</label>
                <Input id="password-input" className="h-5 w-8 ph-4 mt-2" placeholder="••••••••••" />

                <Button
                    className="font-ubuntu fs-20 lh-23 bold c-default h-5 w-8 ph-4 b-primary mt-4"
                    onClick={ () => {} }
                    disabled={ true }
                >
                    Нэвтрэх
                </Button>
            </div>
        </div>
    )
}
