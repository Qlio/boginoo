import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

import { firestore } from '../firebase';

export const UrlRedirect = () => {
    const { firestoreId } = useParams();
    const [ errorMassage, setErrorMessage ] = useState();

    useEffect(() => {
        const getUrl = async () => {
            const doc = await firestore.doc(`urls/${ firestoreId }`).get();

            if (doc.exists) {
                window.location.href = doc.data().originalUrl;
            } else {
                setErrorMessage('404');
            }
        }

        getUrl();
    }, [ firestoreId ])

    if (errorMassage) {
        return (
            <h1>{ errorMassage }</h1>
        )
    }

    return <h1>Please wait...</h1>
}
