export * from './home-default'
export { UrlRedirect } from './url-redirect'
export { Login as LoginPage } from './login'
export { Register as RegisterPage } from './register'
