import React, { useState, useEffect } from 'react';

const useMyCustomState = (initialValue) => {
    return [ value, setValue ] = useState(initialValue);
};

export const FriendListItem = (props) => {
    const [ hello, setHello ] = useMyCustomState(null);
}
