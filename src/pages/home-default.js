import React, { useContext, useState } from 'react';
import { firebaseAuth, firestore } from '../firebase';

import { checkIsValidUrl } from '../validators';
import { Button, Input, History } from '../components/';
import { AuthContext } from '../contexts';
import { useEffect } from 'react';

export const HomeDefault = () => {
    const [ url, setUrl ] = useState('');
    const [ isLoading, setIsLoading ] = useState(false);
    const { user } = useContext(AuthContext);
    const isValidUrl = checkIsValidUrl(url);

    const onClickShotenUrl = async () => {
        setIsLoading(true);

        const urlDoc = await firestore.collection(`users/${ user.uid }/history`).add({
            originalUrl: url,
            createdAt: new Date(),
        }).catch((error) => {
            console.log(error)
        });

        setHistories([{
            id: urlDoc.id,
            originalUrl: url,
        }, ...histories]);

        setIsLoading(false);
        setUrl('');
    }

    const [ histories, setHistories ] = useState([]);
    useEffect(() => {
        firestore.collection(`users/${ user.uid }/history`).orderBy('createdAt', 'desc').onSnapshot((snapshot) => {
            const _histories = [];
            snapshot.forEach((doc) => {
                _histories.push({
                    id: doc.id,
                    ...doc.data(),
                })
            })

            setHistories(_histories);
        })
    }, [ user ]);

    const sendResetEmail = async () => {
        await firebaseAuth.sendPasswordResetEmail('bulgaanest@gmail.com')
    }

    return (
        <div className='h100 flex flex-col justify-center items-center'>
            <div className='mt-5 flex justify-center items-center'>
                <Input className="h-5 w-8 ph-4" placeholder='https://www.web-huudas.mn' value={ url } onChange={ setUrl } />
                <Button
                    className="font-ubuntu fs-20 lh-23 bold c-default h-5 w-8 ph-4 ml-4 b-primary"
                    onClick={ onClickShotenUrl }
                    disabled={ isLoading || !url || !isValidUrl }
                >
                    Богиносгох
                </Button>
                <Button onClick={ sendResetEmail }> reset email</Button>
            </div>
            <History histories={ histories } />
        </div>
    )
}
