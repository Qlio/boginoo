import React, { useContext, useState } from 'react';

import firebase from '../firebase';
import { Button, Input } from '../components/';
import { AuthContext } from '../contexts';

const useInput = (initialvalue = '') => {
    const [ value, setValue ] = useState(initialvalue);

    const resetInput = () => {
        setValue(initialvalue);
    }

    const bind = {
        value,
        onChange: (v) => {
            setValue(v);
        },
    };

    return [ value, bind, resetInput ]
}

export const Register = () => {
    const [ isLoading, setIsLoading ] = useState(false);
    const { user, setUser } = useContext(AuthContext);

    const [ email, emailBind, emailReset ] = useInput();
    const [ password, passwordBind, passwordReset ] = useInput()
    const [ passwordConfirm, passwordConfirmBind, passwordConfirmReset ] = useInput();

    const onSubmit = async (e) => {
        e.preventDefault();

        if (password !== passwordConfirm) {
            passwordReset();
            passwordConfirmReset();
            return alert('Нууц үгс зөрж байна');
        }

        setIsLoading(true);
        try {
            const credential = firebase.auth.EmailAuthProvider.credential(email, password);
            const linkedUser = await user.linkWithCredential(credential);
            setUser(linkedUser);
        } catch (e) {
            console.error(e);
            setIsLoading(false);
        }
    }

    const onReset = (e) => {
        e.preventDefault();

        emailReset();
        passwordReset();
        passwordConfirmReset();
    }

    return (
        <div className='h100 flex flex-col justify-center items-center'>
            <form className="font-ubuntu" onSubmit={ onSubmit }>
                <h1 className="text-center c-primary">Бүртгүүлэх</h1>

                <label className="ph-4" htmlFor="email-input">Цахим хаяг</label>
                <Input id="email-input" className="h-5 w-8 ph-4 mt-2 mb-4" placeholder="name@mail.doman" { ...emailBind } />

                <label className="ph-4" htmlFor="password-input">Нууц үг</label>
                <Input id="password-input" className="h-5 w-8 ph-4 mt-2 mb-4" type="password" placeholder="••••••••••" { ...passwordBind } />

                <label className="ph-4" htmlFor="password-input-confirm">Нууц үг давтана уу</label>
                <Input id="password-input-confirm" className="h-5 w-8 ph-4 mt-2" type="password" placeholder="••••••••••" { ...passwordConfirmBind } />

                <Button
                    className="font-ubuntu fs-20 lh-23 bold c-default h-5 w-8 ph-4 b-primary mt-4"
                    disabled={ isLoading }
                >
                    Бүртгүүлэх
                </Button>
                <a href="/" onClick={ onReset }>Reset</a>
            </form>
        </div>
    )
}
